<?php

/**
 * @file
 * Contains monitoring_icinga.module.
 */

use Drupal\monitoring\Entity\SensorConfig;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function monitoring_icinga_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the monitoring_icinga module.
    case 'help.page.monitoring_icinga':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Monitoring Icinga') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_theme().
 */
function monitoring_icinga_theme() {
  return [
    'monitoring_icinga__config_box' => [
      'variables' => [
        'title' => NULL,
        'description' => NULL,
        'code' => NULL,
        'code_height' => NULL,
      ],
    ],
  ];
}

/**
 * Gets service description for Icinga.
 *
 * This is vital for matching the generated service (sensor) config and
 * submitting passive check.
 *
 * @param \Drupal\monitoring\Entity\SensorConfig $sensor_info
 *   Sensor info object.
 *
 * @return string
 *   Icinga service description.
 */
function monitoring_icinga_service_description(SensorConfig $sensor_info) {
  return $sensor_info->getCategory() . ' - ' . $sensor_info->getLabel() . ' - ' . $sensor_info->id();
}

/**
 * Get icinga status mappings.
 *
 * @return array
 *   Mapped statuses.
 */
function monitoring_icinga_status_codes() {
  return [
    'OK' => 0,
    'WARNING' => 1,
    'CRITICAL' => 2,
    'UNKNOWN' => 3,
  ];
}
